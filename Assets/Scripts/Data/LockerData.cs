﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Data/LockerData", fileName = "LockerData")]
public class LockerData : ScriptableObject
{
    public enum DotsIndexes
    {
        None,
        d00,
        d01,
        d02,
        d10,
        d11,
        d12,
        d20,
        d21,
        d22,
    }
    public DotsIndexes[] trueIndexes = new DotsIndexes[9];

    private void OnValidate()
    {
        if (trueIndexes.Length < 3 || trueIndexes.Length > 9)
        {
            Debug.LogError("Invalid trueindexes length. Please fix");
            return;
        }

        List<DotsIndexes> validateList = new List<DotsIndexes>();

        foreach (var index in trueIndexes)
        {
            if (index == DotsIndexes.None)
            {
                continue;
            }
            if (validateList.Contains(index))
            {
                Debug.LogError($"Trueindexes contains {index} twice or more. Please fix");
            }
            else
            {
                validateList.Add(index);
            }
        }
    }
}
