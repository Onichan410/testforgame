﻿using UnityEngine;

public class SwipeSystem : BaseMonoSystem
{
    [SerializeField] private LineRenderer lineRenderer;

    private Camera camera;
    private int currentLineIndex;

    public override void Init(AppData data)
    {
        base.Init(data);
        camera = Camera.main;
    }

    public void ClickDot(DotView dot)
    {
        if (data.stateData.state.Value != StateData.State.Idle)
        {
            return;
        }
        data.swipeData.state.Value = SwipeData.SwipeState.Began;
        AddDot(dot);
    }

    public void HoverDot(DotView dot)
    {
        if (data.stateData.state.Value != StateData.State.Idle)
        {
            return;
        }
        if (data.swipeData.addedDots.Count == 0)
        {
            return;
        }
        data.swipeData.state.Value = SwipeData.SwipeState.Move;
        AddDot(dot);
    }

    private void AddDot(DotView dot)
    {
        if (data.swipeData.addedDots.Contains(dot))
        {
            UndoSelection(dot);
            return;
        }

        lineRenderer.positionCount++;
        lineRenderer.SetPosition(currentLineIndex,
            new Vector3(dot.transform.position.x, dot.transform.position.y, data.dotsObjectsData.zPosition));

        currentLineIndex++;
        dot.Select();
        data.swipeData.addedDots.Add(dot);
    }
    private void UndoSelection(DotView dot)
    {
        int index = data.swipeData.addedDots.IndexOf(dot);
        lineRenderer.positionCount = index + 1;
        currentLineIndex = index;
        UnselectDots(index);
        data.swipeData.addedDots.RemoveRange(index, data.swipeData.addedDots.Count - index);
    }
    private void UnselectDots(int index)
    {
        for (int i = index; i < data.swipeData.addedDots.Count; i++)
        {
            data.swipeData.addedDots[i].Unselect();
        }
    }
    private void Update()
    {
        if (data.swipeData.state.Value == SwipeData.SwipeState.Began
            || data.swipeData.state.Value == SwipeData.SwipeState.Move)
        {
            if (!Input.GetMouseButton(0))
            {
                EndSwipe();
            }
            Vector3 pos = new Vector3(camera.ScreenToWorldPoint(Input.mousePosition).x,
                camera.ScreenToWorldPoint(Input.mousePosition).y,
                data.dotsObjectsData.zPosition);
            lineRenderer.SetPosition(currentLineIndex, pos);
        }
    }

    private void EndSwipe()
    {
        data.swipeData.state.Value = SwipeData.SwipeState.End;
        currentLineIndex = 0;
        lineRenderer.positionCount = 1;
        UnselectDots(0);
        data.swipeData.addedDots.Clear();
    }
}
