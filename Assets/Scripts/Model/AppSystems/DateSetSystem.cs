﻿using UnityEngine;
using UniRx;

public class DateSetSystem : BaseMonoSystem
{
    [SerializeField] private DateView view;
    public override void Init(AppData data)
    {
        base.Init(data);

        data.dateData.date
            .Subscribe(_ => EnableSetTime())
            .AddTo(this);
    }
    private void EnableSetTime()
    {
        SetDate();

        Observable.Timer(System.TimeSpan.FromSeconds(1))
            .Repeat()
            .Subscribe(_ => SetDate())
            .AddTo(this);
    }
    private void SetDate()
    {
        System.DateTime dateTime = data.dateData.date.Value;
        string time = dateTime.ToString("HH:mm");
        string date = dateTime.ToString("M");
        view.SetDate(time, date);
    }
}
