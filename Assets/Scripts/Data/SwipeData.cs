﻿using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class SwipeData : ScriptableObject
{
    public enum SwipeState
    {
        Idle,
        Began,
        Move,
        End,
    }
    public List<DotView> dotsObjects = new List<DotView>();

    public ReactiveProperty<SwipeState> state = new ReactiveProperty<SwipeState>();

    public List<DotView> addedDots = new List<DotView>();
}
