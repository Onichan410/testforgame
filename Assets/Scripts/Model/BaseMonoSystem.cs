﻿using UnityEngine;

public class BaseMonoSystem : MonoBehaviour
{
    internal AppData data;
    public virtual void Init(AppData data)
    {
        this.data = data;
    }
}
