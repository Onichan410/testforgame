﻿using UnityEngine;
using TMPro;

public class DateView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI timeText;
    [SerializeField] private TextMeshProUGUI dateText;

    public void SetDate(string time, string date)
    {
        timeText.text = time;
        dateText.text = date;
    }
}
