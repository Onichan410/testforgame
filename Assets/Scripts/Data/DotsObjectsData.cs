﻿using UnityEngine;

[CreateAssetMenu(menuName = "Data/DotsObjectsData", fileName = "DotsObjectsData")]
public class DotsObjectsData : ScriptableObject
{
    public GameObject dotsPrefab;
    public int countOfDots;

    public float zPosition;

}
