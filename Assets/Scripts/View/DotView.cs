﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class DotView : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler
{
    [SerializeField] private Image dot;
    [SerializeField] private Image circle;

    [SerializeField] private float scale;
    [SerializeField] private float timeToScale;
    [SerializeField] private float timeToColor;

    private LockerData.DotsIndexes index;
    private SwipeSystem swipeSystem;

    public LockerData.DotsIndexes Index => index;

    public void Init(LockerData.DotsIndexes index, SwipeSystem swipeSystem)
    {
        this.index = index;
        this.swipeSystem = swipeSystem;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        swipeSystem.ClickDot(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        swipeSystem.HoverDot(this);
    }

    public async void Select()
    {
        circle.gameObject.SetActive(true);
        await circle.transform.DOScale(new Vector3(scale, scale, 1), timeToScale).AsyncWaitForCompletion();
    }

    public async void Unselect()
    {
        await circle.transform.DOScale(new Vector3(1, 1, 1), timeToScale).AsyncWaitForCompletion();
        circle.gameObject.SetActive(false);
    }

    public void ShowResult(bool isCorrect)
    {
        if (isCorrect)
        {
            dot.DOColor(Color.green, timeToColor);
        }
        else
        {
            dot.DOColor(Color.red, timeToColor);
        }
    }
    public void ResetColor()
    {
        dot.DOColor(Color.white, timeToColor);
    }
}
