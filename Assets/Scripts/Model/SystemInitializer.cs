﻿using UnityEngine;

public class SystemInitializer : MonoBehaviour
{
    public AppData data;

    private void Awake()
    {
        InitApp();
    }

    private void InitApp()
    {
        GetComponent<DataInitializer>().InitData(data);

        BaseMonoSystem[] systems = GameObject.FindObjectsOfType<BaseMonoSystem>();

        foreach (var system in systems)
        {
            system.Init(data);
        }

        data.stateData.state.Value = StateData.State.Idle;
    }
}
