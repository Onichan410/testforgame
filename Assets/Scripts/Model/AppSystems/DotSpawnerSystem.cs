﻿using UnityEngine;
using UniRx;
using UnityEngine.UI;

public class DotSpawnerSystem : BaseMonoSystem
{
    [SerializeField] private GridLayoutGroup grid;
    public override void Init(AppData data)
    {
        base.Init(data);

        data.stateData.state
            .Where(x => x == StateData.State.Initializing)
            .Subscribe(_ => SpawnDots());
    }

    private void SpawnDots()
    {
        SwipeSystem swipeSystem = FindObjectOfType<SwipeSystem>();
        for (int i = 0; i < data.dotsObjectsData.countOfDots; i++)
        {
            DotView dot = Instantiate(data.dotsObjectsData.dotsPrefab, grid.transform).GetComponent<DotView>();
            dot.Init((LockerData.DotsIndexes)(i + 1), swipeSystem);
            data.swipeData.dotsObjects.Add(dot);
        }
    }
}
