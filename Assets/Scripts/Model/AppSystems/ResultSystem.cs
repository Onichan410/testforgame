﻿using UniRx;
using MoonSharp.Interpreter;

public class ResultSystem : BaseMonoSystem
{
    private Script script;
    public override void Init(AppData data)
    {
        base.Init(data);

        data.swipeData.state
            .Where(x => x == SwipeData.SwipeState.End)
            .Subscribe(_ => Prepare())
            .AddTo(this);

        SetupLua();
    }

    private void SetupLua()
    {
        string scriptCode = @"    
		function check (correctIndexes, userIndexes)
            if #correctIndexes ~= #userIndexes then
                return false
            end
            for i = 1, #correctIndexes do 
                if correctIndexes[i] ~= userIndexes[i] then
                    return false
                end
            end
            return true
        end";

        script = new Script();

        script.DoString(scriptCode);
    }

    private void Prepare()
    {
        LockerData.DotsIndexes[] indexes = new LockerData.DotsIndexes[data.swipeData.addedDots.Count];

        for (int i = 0; i < indexes.Length; i++)
        {
            indexes[i] = data.swipeData.addedDots[i].Index;
        }

        Check(indexes, data.lockerData.trueIndexes);
    }

    private void Check(LockerData.DotsIndexes[] user, LockerData.DotsIndexes[] correct)
    {
        data.stateData.state.Value = StateData.State.Result;

        DynValue res = script.Call(script.Globals["check"], new object[] {correct, user});

        ShowResult(res.Boolean);
    }

    private void ShowResult(bool isCorrect)
    {
        foreach (var dotView in data.swipeData.dotsObjects)
        {
            dotView.ShowResult(isCorrect);
        }

        Observable.Timer(System.TimeSpan.FromSeconds(3))
            .Subscribe(_ => ResetState())
            .AddTo(this);
    }

    private void ResetState()
    {
        data.stateData.state.Value = StateData.State.Idle;
        data.swipeData.state.Value = SwipeData.SwipeState.Idle;

        foreach (var dotView in data.swipeData.dotsObjects)
        {
            dotView.ResetColor();
        }
    }
}
