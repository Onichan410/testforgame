﻿using UniRx;

public class DateGetSystem : BaseMonoSystem
{
    public override void Init(AppData data)
    {
        base.Init(data);

        data.stateData.state
            .Where(x => x == StateData.State.Idle)
            .Subscribe(_ => StartGetDate())
            .AddTo(this);
    }

    private void StartGetDate()
    {
        GetDate();

        Observable.Timer(System.TimeSpan.FromSeconds(1))
            .Repeat()
            .Subscribe(_ => GetDate())
            .AddTo(this);
    }

    private void GetDate()
    {
        data.dateData.date.Value = System.DateTime.Now;
    }
}
