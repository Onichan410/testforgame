﻿using UnityEngine;

public class DataInitializer : MonoBehaviour
{
    public void InitData(AppData data)
    {
        data.stateData = ScriptableObject.CreateInstance<StateData>();
        data.swipeData = ScriptableObject.CreateInstance<SwipeData>();
    }
}
