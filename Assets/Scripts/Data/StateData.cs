﻿using UnityEngine;
using UniRx;

public class StateData : ScriptableObject
{
    public enum State
    {
        Initializing,
        Idle,
        Result,
    }
    public ReactiveProperty<State> state = new ReactiveProperty<State>();
}
