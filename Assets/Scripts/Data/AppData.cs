﻿using UnityEngine;

[CreateAssetMenu(menuName = "Data/AppData", fileName = "AppData")]
public class AppData : ScriptableObject
{
    public DateData dateData;
    public LockerData lockerData;
    public DotsObjectsData dotsObjectsData;

    public StateData stateData;
    public SwipeData swipeData;

    private void OnValidate()
    {
        if (dateData == null)
        {
            Debug.LogWarning("DateData == null");
        }
        if (lockerData == null)
        {
            Debug.LogWarning("LockerData == null");
        }
        if (dotsObjectsData == null)
        {
            Debug.LogWarning("DotsObjectsData == null");
        }
    }
}
