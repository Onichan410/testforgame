﻿using UnityEngine;
using System;
using UniRx;

[CreateAssetMenu(menuName = "Data/DateData", fileName = "DateData")]
public class DateData : ScriptableObject
{
    public ReactiveProperty<DateTime> date = new ReactiveProperty<DateTime>(default);
}
